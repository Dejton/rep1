// const cardFirst = document.querySelector('.card.first .top');
// const cardSecond = document.querySelector('.card.second .top');
// const cardThird = document.querySelector('.card.third .top');
// const cardFourth = document.querySelector('.card.fourth .top');
// const cardFifth = document.querySelector('.card.fifth .top');
// const cardSixth = document.querySelector('.card.sixth .top');
// const cardSeventh = document.querySelector('.card.seventh .top');
// const cardEighth = document.querySelector('.card.eighth .top');
// const cardNinth = document.querySelector('.card.ninth .top');
// const arrowLeft = document.querySelector('.left');
// const arrowRight = document.querySelector('.right');
const wrap = document.querySelector('.wrap');

// let position = 49.2;

// document.onkeydown = (e) => {
//     if (e.key === 'ArrowLeft') {
//         moveLeft()
//     } else if (e.key === 'ArrowRight') {
//         moveRight();
//     }
// };

// arrowLeft.addEventListener('click', function () {
//     moveLeft();
// });

// arrowRight.addEventListener('click', function () {
//     moveRight();
// });

// function moveLeft() {
//     position += 11;
//     wrap.style.left = position + "%";
// }

// function moveRight() {
//     position -= 11;
//     wrap.style.left = position + "%";
//     wrap.style.transition = ".4s";
// }

// cardFirst.addEventListener('click', function () {
//     cardFirst.classList.toggle('active');
//     cardSecond.classList.remove('active');
//     cardThird.classList.remove('active');
//     cardFourth.classList.remove('active');
//     cardFifth.classList.remove('active');
//     cardSixth.classList.remove('active');
//     cardSeventh.classList.remove('active');
//     cardEighth.classList.remove('active');
//     cardNinth.classList.remove('active');
// })

// cardSecond.addEventListener('click', function () {
//     cardFirst.classList.remove('active');
//     cardSecond.classList.toggle('active');
//     cardThird.classList.remove('active');
//     cardFourth.classList.remove('active');
//     cardFifth.classList.remove('active');
//     cardSixth.classList.remove('active');
//     cardSeventh.classList.remove('active');
//     cardEighth.classList.remove('active');
//     cardNinth.classList.remove('active');
// })
// cardThird.addEventListener('click', function () {
//     cardFirst.classList.remove('active');
//     cardSecond.classList.remove('active');
//     cardThird.classList.toggle('active');
//     cardFourth.classList.remove('active');
//     cardFifth.classList.remove('active');
//     cardSixth.classList.remove('active');
//     cardSeventh.classList.remove('active');
//     cardEighth.classList.remove('active');
//     cardNinth.classList.remove('active');
// })
// cardFourth.addEventListener('click', function () {
//     cardFirst.classList.remove('active');
//     cardSecond.classList.remove('active');
//     cardThird.classList.remove('active');
//     cardFourth.classList.toggle('active');
//     cardFifth.classList.remove('active');
//     cardSixth.classList.remove('active');
//     cardSeventh.classList.remove('active');
//     cardEighth.classList.remove('active');
//     cardNinth.classList.remove('active');
// })
// cardFifth.addEventListener('click', function () {
//     cardFirst.classList.remove('active');
//     cardSecond.classList.remove('active');
//     cardThird.classList.remove('active');
//     cardFourth.classList.remove('active');
//     cardFifth.classList.toggle('active');
//     cardSixth.classList.remove('active');
//     cardSeventh.classList.remove('active');
//     cardEighth.classList.remove('active');
//     cardNinth.classList.remove('active');
// })
// cardSixth.addEventListener('click', function () {
//     cardFirst.classList.remove('active');
//     cardSecond.classList.remove('active');
//     cardThird.classList.remove('active');
//     cardFourth.classList.remove('active');
//     cardFifth.classList.remove('active');
//     cardSixth.classList.toggle('active');
//     cardSeventh.classList.remove('active');
//     cardEighth.classList.remove('active');
//     cardNinth.classList.remove('active');
// })
// cardSeventh.addEventListener('click', function () {
//     cardFirst.classList.remove('active');
//     cardSecond.classList.remove('active');
//     cardThird.classList.remove('active');
//     cardFourth.classList.remove('active');
//     cardFifth.classList.remove('active');
//     cardSixth.classList.remove('active');
//     cardSeventh.classList.toggle('active');
//     cardEighth.classList.remove('active');
//     cardNinth.classList.remove('active');
// })
// cardEighth.addEventListener('click', function () {
//     cardFirst.classList.remove('active');
//     cardSecond.classList.remove('active');
//     cardThird.classList.remove('active');
//     cardFourth.classList.remove('active');
//     cardFifth.classList.remove('active');
//     cardSixth.classList.remove('active');
//     cardSeventh.classList.remove('active');
//     cardEighth.classList.toggle('active');
//     cardNinth.classList.remove('active');
// })
// cardNinth.addEventListener('click', function () {
//     cardFirst.classList.remove('active');
//     cardSecond.classList.remove('active');
//     cardThird.classList.remove('active');
//     cardFourth.classList.remove('active');
//     cardFifth.classList.remove('active');
//     cardSixth.classList.remove('active');
//     cardSeventh.classList.remove('active');
//     cardEighth.classList.remove('active');
//     cardNinth.classList.toggle('active');
// })


//* jQuery *//

let position = 49.2


function moveLeft() {
    position += 11;
    wrap.style.left = position + "%";
}

function moveRight() {
    position -= 11;
    wrap.style.left = position + "%";
    wrap.style.transition = ".4s";
}


$('.left').on('click', moveLeft)
$('.right').on('click', moveRight)

$(".card.first .top").on('click', function () {
    $('.card.first .top').toggleClass('active')
    $('.card.fifth .top, .card.second .top, .card.third .top, .card.fourth .top, .card.sixth .top, .card.seventh .top, .card.eighth .top, .card.ninth .top').removeClass('active')
})
$(".card.second .top").on('click', function () {
    $('.card.second .top').toggleClass('active')
    $('.card.first .top, .card.fifth .top, .card.third .top, .card.fourth .top, .card.sixth .top, .card.seventh .top, .card.eighth .top, .card.ninth .top').removeClass('active')
})
$(".card.third .top").on('click', function () {
    $('.card.third .top').toggleClass('active')
    $('.card.first .top, .card.second .top, .card.fifth .top, .card.fourth .top, .card.sixth .top, .card.seventh .top, .card.eighth .top, .card.ninth .top').removeClass('active')
})
$(".card.fourth .top").on('click', function () {
    $('.card.fourth .top').toggleClass('active')
    $('.card.first .top, .card.second .top, .card.third .top, .card.fifth .top, .card.sixth .top, .card.seventh .top, .card.eighth .top, .card.ninth .top').removeClass('active')
})
$(".card.fifth .top").on('click', function () {
    $('.card.fifth .top').toggleClass('active')
    $('.card.first .top, .card.second .top, .card.third .top, .card.fourth .top, .card.sixth .top, .card.seventh .top, .card.eighth .top, .card.ninth .top').removeClass('active')
})
$('.card.sixth .top').on('click', function () {
    $('.card.sixth .top').toggleClass('active')
    $('.card.first .top, .card.second .top, .card.third .top, .card.fourth .top, .card.fifth .top, .card.seventh .top, .card.eighth .top, .card.ninth .top').removeClass('active')
})
$('.card.seventh .top').on('click', function () {
    $('.card.first .top, .card.second .top, .card.third .top, .card.fourth .top, .card.sixth .top, .card.fifth .top, .card.eighth .top, .card.ninth .top').removeClass('active')
    $('.card.seventh .top').toggleClass('active')
})
$('.card.eighth .top').on('click', function () {
    $('.card.first .top, .card.second .top, .card.third .top, .card.fourth .top, .card.sixth .top, .card.fifth .top, card.seventh .top, .card.ninth .top').removeClass('active')
    $('.card.eighth .top').toggleClass('active')
})
$('.card.ninth .top').on('click', function () {
    $('.card.first .top, .card.second .top, .card.third .top, .card.fourth .top, .card.sixth .top, .card.fifth .top, .card.eighth .top, .card.seventh .top').removeClass('active')
    $('.card.ninth .top').toggleClass('active')
})